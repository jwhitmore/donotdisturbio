﻿using System;

namespace DoNotDisturbIO
{
    [Serializable]
    class LEDColor
    {
        public byte red { get; set; }
        public byte green { get; set; }
        public byte blue { get; set; }

        public LEDColor()
        {
            red = green = blue = 0;
        }
    }
}
