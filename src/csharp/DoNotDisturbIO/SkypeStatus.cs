﻿using System;
using System.Web.Script.Serialization;
using Microsoft.Lync.Model;

namespace DoNotDisturbIO
{
    public enum SkypeAvailability
    {
        None = ContactAvailability.None,
        Away = ContactAvailability.Away,
        Busy = ContactAvailability.Busy,
        DoNotDisturb = ContactAvailability.DoNotDisturb,
        Free = ContactAvailability.Free,
        Offline = ContactAvailability.Offline,
        IncomingCall = 99999
    };

    [Serializable]
    class SkypeStatus
    {
        [ScriptIgnore]
        private SkypeAvailability skypeAvailability;
        public SkypeAvailability SkypeAvailability
        {
            get { return skypeAvailability; }
            set
            {
                if (value.GetType() == typeof(SkypeAvailability))
                {
                    skypeAvailability = value;
                }
                else
                {
                    skypeAvailability = FromContactAvailability((ContactAvailability)value);
                }
                
            }
        }
        public Animations Animation;
        public LEDColor Color;
        public int Delay;
        public bool TurnOff;
        [ScriptIgnore]
        public string DisplayMember => SkypeAvailability.ToString();
        [ScriptIgnore]
        public SkypeStatus ValueMember => this;

        public SkypeStatus()
        {
            SkypeAvailability = SkypeAvailability.None;
            Animation = Animations.Solid;
            Color = new LEDColor();
            Delay = 60;
            TurnOff = false;
        }

        public static SkypeAvailability FromContactAvailability(ContactAvailability contactAvailability)
        {
            switch (contactAvailability)
            {
                case ContactAvailability.Away:
                    return SkypeAvailability.Away;
                case ContactAvailability.Busy:
                    return SkypeAvailability.Busy;
                case ContactAvailability.BusyIdle:
                    return SkypeAvailability.Busy;
                case ContactAvailability.DoNotDisturb:
                    return SkypeAvailability.DoNotDisturb;
                case ContactAvailability.Free:
                    return SkypeAvailability.Free;
                case ContactAvailability.FreeIdle:
                    return SkypeAvailability.Free;
                case ContactAvailability.Invalid:
                    return SkypeAvailability.None;
                case ContactAvailability.Offline:
                    return SkypeAvailability.Offline;
                case ContactAvailability.TemporarilyAway:
                    return SkypeAvailability.Away;
                default:
                    return SkypeAvailability.None;
            }            
        }

        // returns: animation,red,green,blue,speed_delay,turn_off
        // example: 8,155,255,10,30,1
        public override string ToString()
        {
            return String.Format("{0},{1},{2},{3},{4},{5}", (int)Animation, Color.red, Color.green, Color.blue, Delay, (TurnOff ? 1 : 0));
        }

        public static string TurnOffMessage()
        {
            return new SkypeStatus()
            {
                TurnOff = true
            }.ToString();
        }

    }
}
