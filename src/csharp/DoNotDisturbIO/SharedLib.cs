#if SHARED_LIB_H
#define public
#else
namespace DoNotDisturbIO
{
    static class SharedLib
    {
#endif

        public static char START_CHAR = '<';
        public static char END_CHAR = '>';
#if !SHARED_LIB_H
    }
#endif
    public enum Animations
        {
            None = 0
          , Solid = 1 << 0
          , RainbowCycle = 1 << 1
          , RunningLights = 1 << 2
          , PoliceStrobe = 1 << 3
          , PoliceStrobe2 = 1 << 4
          , ShootingStar = 1 << 5
          , FillFromLeft = 1 << 6
          , FillFromRight = 1 << 7
          , BreatheRainbow = 1 << 8
          , FadeInOut = 1 << 9
          , Sparkle = 1 << 10
          , SnowSparkle = 1 << 11
        };

#if SHARED_LIB_H
#undef public
#else
    static class AnimationInfo
    {
        public static Animations AnimationsWithRGB = (
            Animations.Solid | 
            Animations.RunningLights | 
            Animations.ShootingStar |
            Animations.FillFromLeft | 
            Animations.FillFromRight | 
            Animations.FadeInOut | 
            Animations.Sparkle |
            Animations.SnowSparkle
            );
        public static Animations AnimationsWithDelay = (
            Animations.RainbowCycle | 
            Animations.RunningLights | 
            Animations.PoliceStrobe | 
            Animations.PoliceStrobe2 | 
            Animations.ShootingStar | 
            Animations.FillFromLeft | 
            Animations.FillFromRight | 
            Animations.BreatheRainbow | 
            Animations.FadeInOut |
            Animations.Sparkle |
            Animations.SnowSparkle
            );
    }
}
#endif