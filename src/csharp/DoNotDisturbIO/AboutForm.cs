﻿using System;
using System.Windows.Forms;
using System.Reflection;

namespace DoNotDisturbIO
{
    public partial class AboutForm : Form
    {
        public AboutForm()
        {
            InitializeComponent();
        }

        private void AboutForm_Load(object sender, EventArgs e)
        {
            lblVersion.Text = "DoNotDisturbIO " + GetRunningVersion().ToString();
        }

        private Version GetRunningVersion()
        {
            try
            {
                return System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion;
            }
            catch
            {
                return Assembly.GetExecutingAssembly().GetName().Version;
            }
        }

        private void paypalButton_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.paypal.me/jjwhitmore");
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pbGCSync_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://dndio.justinwhitmore.com");
        }

        private void lblVersion_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://dndio.justinwhitmore.com/page/changelog/");
        }
    }
}
