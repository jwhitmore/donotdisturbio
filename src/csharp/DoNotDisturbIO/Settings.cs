﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoNotDisturbIO
{
    class Settings
    {
        public static string FILENAME = String.Format("{0}\\DoNotDisturb.json", System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));

        private static Settings _instance;

        public static Settings Instance
        {
            get
            {
                if (_instance == null) { _instance = new Settings(); }
                return _instance;
            }
            set
            {
                _instance = value;
            }
        }

        public List<SkypeStatus> SkypeStatuses;
        public DeviceInfo DeviceInfo;

        public bool MinimizeToTray;
        public bool WinStart;

        public Settings()
        {
            SkypeStatuses = new List<SkypeStatus>();
            DeviceInfo = new DeviceInfo();
            MinimizeToTray = true;
            WinStart = true;
        }

        public void SaveSettings()
        {
            Serialize.WriteToJsonFile<Settings>(Settings.FILENAME, Settings.Instance);
        }

        public void LoadSettings()
        {
            try
            {
                Settings.Instance.SkypeStatuses.Clear();
                Settings.Instance = Serialize.ReadFromJsonFile<Settings>(Settings.FILENAME);
            }
            catch (Exception ex)
            {
                Settings.Instance.SkypeStatuses.Clear();
                foreach (var status in Enum.GetValues(typeof(SkypeAvailability)))
                {
                    SkypeStatus skypeStatus = new SkypeStatus();
                    skypeStatus.SkypeAvailability = (SkypeAvailability)status;
                    Settings.Instance.SkypeStatuses.Add(skypeStatus);
                }
                SaveSettings();
            }
        }
    }
}
