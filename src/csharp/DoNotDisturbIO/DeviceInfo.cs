﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Management;

namespace DoNotDisturbIO
{

    public enum DEVICE_TYPE
    {
        COM = 1,
        BT = 2
    }

    [Serializable]
    class DeviceInfo
    {
        
        public string Name;
        public string Description;
        public DEVICE_TYPE DeviceType;

        [ScriptIgnore]
        public string DisplayMember => Description;
        [ScriptIgnore]
        public DeviceInfo ValueMember => this;
    }

    class ComPorts
    { 
        // Method to prepare the WMI query connection options.  
        public static ConnectionOptions PrepareOptions()
        {
            ConnectionOptions options = new ConnectionOptions();
            options.Impersonation = ImpersonationLevel.Impersonate;
            options.Authentication = AuthenticationLevel.Default;
            options.EnablePrivileges = true;
            return options;
        }

        // Method to prepare WMI query management scope.
        public static ManagementScope PrepareScope(string machineName, ConnectionOptions options, string path)
        {
            ManagementScope scope = new ManagementScope();
            scope.Path = new ManagementPath(@"\\" + machineName + path);
            scope.Options = options;
            scope.Connect();
            return scope;
        }

        // Method to retrieve the list of all COM ports.
        public static List<DeviceInfo> GetAvailablePorts()
        {
            // SerialPort.GetPortNames() // Another way to get list of ports
            List<DeviceInfo> portList = new List<DeviceInfo>();

            // Prepare the query and searcher objects.
            ObjectQuery objectQuery = new ObjectQuery("SELECT * FROM Win32_PnPEntity"); //  WHERE ClassGuid=\"{4d36e978-e325-11ce-bfc1-08002be10318}\"
            ManagementObjectSearcher portSearcher = new ManagementObjectSearcher(ComPorts.PrepareScope(Environment.MachineName, ComPorts.PrepareOptions(), "\\root\\CIMV2"), objectQuery); // \\root\\CIMV2 or \\root\\WMI

            using (portSearcher)
            {
                string caption = null;
                // Invoke the searcher and search through each management object for a COM port.
                foreach (ManagementObject currentObject in portSearcher.Get())
                {
                    if (currentObject != null)
                    {
                        Console.WriteLine(String.Format("DeviceID={0}\nName={1}\nPNPDeviceID={2}\nDescription={3}\nCaption={4}"
                            , (currentObject.GetPropertyValue("DeviceID") != null ? currentObject.GetPropertyValue("DeviceID").ToString() : String.Empty)
                            , (currentObject.GetPropertyValue("Name") != null ? currentObject.GetPropertyValue("Name").ToString() : String.Empty)
                            , (currentObject.GetPropertyValue("PNPDeviceID") != null ? currentObject.GetPropertyValue("PNPDeviceID").ToString() : String.Empty)
                            , (currentObject.GetPropertyValue("Description") != null ? currentObject.GetPropertyValue("Description").ToString() : String.Empty)
                            , (currentObject.GetPropertyValue("Caption") != null ? currentObject.GetPropertyValue("Caption").ToString() : String.Empty)));
                        object currentObjectCaption = currentObject["Caption"];
                        if (currentObjectCaption != null)
                        {
                            caption = currentObjectCaption.ToString();
                            if (caption.Contains("(COM"))
                            {
                                //if (caption.Contains("COM9")) { continue; }
                                DeviceInfo portInfo = new DeviceInfo();
                                if (caption.Contains("COM"))
                                {
                                    portInfo.Name = caption.Substring(caption.LastIndexOf("(COM")).Replace("(", string.Empty).Replace(")", string.Empty);
                                }
                                else
                                {
                                    portInfo.Name = caption;
                                }
                                portInfo.Description = caption;
                                portInfo.DeviceType = DEVICE_TYPE.COM;
                                portList.Add(portInfo);
                            }
                        }
                    }
                }
            }

            return portList;
        }
    }
}
