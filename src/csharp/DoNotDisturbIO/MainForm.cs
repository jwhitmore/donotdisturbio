﻿using System;
using System.Threading;
using System.IO.Ports;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.Lync.Model;
using Microsoft.Lync.Model.Conversation;
using Microsoft.Lync.Model.Conversation.AudioVideo;
using AutoUpdaterDotNET;

namespace DoNotDisturbIO
{
    public partial class MainForm : Form
    {
        private static string UPDATE_URL = "https://donotdisturbio.justinwhitmore.com/dndio_updates.xml";
        private volatile bool RUNNING = true;
        private bool starting = true;
        private LyncClient _skypeClient;        
        SerialPort serialPort = new SerialPort();
        List<DeviceInfo> comPortInfoList = new List<DeviceInfo>();

        public MainForm()
        {
            InitializeComponent();            
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {

                AutoUpdater.ApplicationExitEvent += AutoUpdater_ApplicationExitEvent;

                AutoUpdater.Start(UPDATE_URL);

                Settings.Instance.LoadSettings();
                statusTimer.Interval = 8000;

                comPortInfoList = ComPorts.GetAvailablePorts();
                cboComPort.DataSource = comPortInfoList;
                cboComPort.DisplayMember = "DisplayMember";
                cboComPort.ValueMember = "ValueMember";

                cboAnimations.DataSource = Enum.GetValues(typeof(Animations));
                updateTimer.Tick += new EventHandler(UpdateTimerTick);

                // 9600, Parity.None, 8, StopBits.None
                serialPort.BaudRate = 9600;
                serialPort.Parity = Parity.None;
                serialPort.DataBits = 8;
                serialPort.StopBits = StopBits.One;

                foreach (var item in cboComPort.Items)
                {
                    DeviceInfo deviceInfo = (DeviceInfo)item;
                    if (deviceInfo.Name == Settings.Instance.DeviceInfo.Name)
                    {
                        cboComPort.SelectedItem = item;
                        break;
                    }
                }

                lsbxSkypeStatus.DataSource = Settings.Instance.SkypeStatuses;
                lsbxSkypeStatus.DisplayMember = "DisplayMember";
                lsbxSkypeStatus.ValueMember = "ValueMember";
                minimizeToTrayToolStripMenuItem.Checked = Settings.Instance.MinimizeToTray;
                startWithWindowsToolStripMenuItem.Checked = Settings.Instance.WinStart;

                startWithWindows();

                this.Resize += new EventHandler(mainForm_Resize);
                trayIcon.DoubleClick += new EventHandler(trayIcon_MouseDoubleClick);
                this.FormClosing += new FormClosingEventHandler(mainForm_FormClosing);

                tslStatus.Text = String.Empty;
                trayIcon.Visible = true;
                trayIcon.Text = "DoNotDisturbIO";

                starting = false;
                statusSetup();
                triggerCOM();
                saveSettings();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        protected void UpdateTimerStart()
        {
            updateTimer.Stop();
            updateTimer.Interval = ((60 * 1000) * (12 * 60)); // 12 hours
            updateTimer.Enabled = true;
            updateTimer.Start();
        }

        private void UpdateTimerTick(object sender, EventArgs e)
        {
            try
            {
                updateTimer.Enabled = false;
                updateTimer.Stop();
                AutoUpdater.Start(UPDATE_URL);
                UpdateTimerStart();
            }
            catch (Exception ex)
            {
                updateStatus(ex.Message);
            }
        }

        private void AutoUpdater_ApplicationExitEvent()
        {
            RUNNING = false;
            Thread.Sleep(1000);
            this.Close();
        }

        private void mainForm_FormClosing(Object sender, FormClosingEventArgs e)
        {
            if (RUNNING && Settings.Instance.MinimizeToTray)
            {
                showToolTip(5000, "Still Running", "DoNotDisturbIO has been minimized to tray\nUse File->Exit to exit DoNotDisturbIO", ToolTipIcon.Info, true);
                e.Cancel = true;
                this.Hide();
            }
            else
            {
                try
                {
                    if (trayIcon != null)
                    {
                        trayIcon.Visible = false;
                        if (trayIcon.Icon != null)
                        {
                            trayIcon.Icon.Dispose();
                        }
                        trayIcon.Dispose();
                    }
                    writeMessage(SkypeStatus.TurnOffMessage());
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private void showToolTip(int milliseconds, string title, string message, ToolTipIcon icon = ToolTipIcon.Info, bool forceShow = false)
        {
            trayIcon.ShowBalloonTip(milliseconds, title, message, icon); 
        }

        protected override void OnClosed(EventArgs e)
        {
            saveSettings();
        }

        private void mainForm_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                if (Settings.Instance.MinimizeToTray)
                {
                    showToolTip(5000, "Minimized", "DoNotDisturbIO has been minimized to tray", ToolTipIcon.Info, true);
                    this.Hide();
                }
            }
        }

        private void lsbxSkypeStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboAnimations.SelectedItem == null) { return; }
                if (lsbxSkypeStatus.SelectedValue == null) { return; }
                SkypeStatus skypeStatus = (SkypeStatus)lsbxSkypeStatus.SelectedValue;
                cboAnimations.SelectedItem = skypeStatus.Animation;
                nudRed.Value = skypeStatus.Color.red;
                nudGreen.Value = skypeStatus.Color.green;
                nudBlue.Value = skypeStatus.Color.blue;
                nudDelay.Value = skypeStatus.Delay;
                cbTurnOff.Checked = skypeStatus.TurnOff;
            }
            catch (Exception)
            {
                return;
            }
        }

        private void cboAnimations_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboAnimations.SelectedValue == null) { return; }
                if (lsbxSkypeStatus.SelectedValue == null) { return; }
                SkypeStatus skypeStatus = (SkypeStatus)lsbxSkypeStatus.SelectedValue;
                skypeStatus.Animation = (Animations)cboAnimations.SelectedValue;
                nudRed.Enabled = ((AnimationInfo.AnimationsWithRGB & skypeStatus.Animation) == skypeStatus.Animation && !skypeStatus.TurnOff);
                nudGreen.Enabled = ((AnimationInfo.AnimationsWithRGB & skypeStatus.Animation) == skypeStatus.Animation && !skypeStatus.TurnOff);
                nudBlue.Enabled = ((AnimationInfo.AnimationsWithRGB & skypeStatus.Animation) == skypeStatus.Animation && !skypeStatus.TurnOff);
                btnColorPicker.Enabled = ((AnimationInfo.AnimationsWithRGB & skypeStatus.Animation) == skypeStatus.Animation && !skypeStatus.TurnOff);
                nudDelay.Enabled = ((AnimationInfo.AnimationsWithDelay & skypeStatus.Animation) == skypeStatus.Animation && !skypeStatus.TurnOff);

            }
            catch (Exception)
            {
                return;
            }
        }


        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!this.Visible)
            {
                this.Show();
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void checkForUpdatesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                updateTimer.Enabled = false;
                updateTimer.Stop();
                AutoUpdater.Start(UPDATE_URL);
                UpdateTimerStart();
            }
            catch (Exception ex)
            {
                updateStatus(ex.Message);
            }
        }

        private void btnColorPicker_Click(object sender, EventArgs e)
        {
            colorPicker.Color = Color.FromArgb((int)nudRed.Value, (int)nudGreen.Value, (int)nudBlue.Value);
            if (colorPicker.ShowDialog() == DialogResult.OK)
            {
                nudRed.Value = colorPicker.Color.R;
                nudGreen.Value = colorPicker.Color.G;
                nudBlue.Value = colorPicker.Color.B;
            }
        }

        private void nudRed_ValueChanged(object sender, EventArgs e)
        {
            setRGBDValue((byte)nudRed.Value, 'R');
        }

        private void nudGreen_ValueChanged(object sender, EventArgs e)
        {
            setRGBDValue((byte)nudGreen.Value, 'G');
        }

        private void nudBlue_ValueChanged(object sender, EventArgs e)
        {
            setRGBDValue((byte)nudBlue.Value, 'B');
        }

        private void nudDelay_ValueChanged(object sender, EventArgs e)
        {
            setRGBDValue((int)nudDelay.Value, 'D');
        }

        private void cboComPort_SelectedIndexChanged(object sender, EventArgs e)
        {
            triggerCOM(false);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                starting = true;
                comPortInfoList.Clear();
                comPortInfoList = ComPorts.GetAvailablePorts();
                cboComPort.DataSource = comPortInfoList;
                cboComPort.DisplayMember = "DisplayMember";
                cboComPort.ValueMember = "ValueMember";
                foreach (var item in cboComPort.Items)
                {
                    DeviceInfo deviceInfo = (DeviceInfo)item;
                    if (deviceInfo.Name == Settings.Instance.DeviceInfo.Name)
                    {
                        cboComPort.SelectedItem = item;
                        break;
                    }
                }
                starting = false;
                triggerCOM();
            }
            catch (Exception ex)
            {
                updateStatus(ex.Message);
            }
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                Animations animations;
                SkypeAvailability skypeAvailability;
                Enum.TryParse(cboAnimations.SelectedValue.ToString(), out animations);
                Enum.TryParse(lsbxSkypeStatus.SelectedValue.ToString(), out skypeAvailability);
                SkypeStatus status = (SkypeStatus)lsbxSkypeStatus.SelectedItem;

                SkypeStatus skypeStatus = new SkypeStatus();
                skypeStatus.Color.red = (byte)nudRed.Value;
                skypeStatus.Color.green = (byte)nudGreen.Value;
                skypeStatus.Color.blue = (byte)nudBlue.Value;
                skypeStatus.Animation = (Animations)cboAnimations.SelectedItem;
                skypeStatus.Delay = (int)nudDelay.Value;
                skypeStatus.TurnOff = cbTurnOff.Checked;

                writeMessage(skypeStatus.ToString());

            }
            catch (Exception ex)
            {
                updateStatus(ex.Message);
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveSettings();
        }

        private void tsbtnSkype_Click(object sender, EventArgs e)
        {
            statusSetup();
        }

        private void tsbtnCOM_Click(object sender, EventArgs e)
        {
            triggerCOM(serialPort.IsOpen);
        }

        private void tslStatus_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(tslStatus.Text))
            {
                MessageBox.Show(tslStatus.Text);
            }
        }

        private void cbTurnOff_CheckedChanged(object sender, EventArgs e)
        {
            SkypeStatus skypeStatus = (SkypeStatus)lsbxSkypeStatus.SelectedValue;
            skypeStatus.TurnOff = cbTurnOff.Checked;
            nudRed.Enabled = !skypeStatus.TurnOff;
            nudGreen.Enabled = !skypeStatus.TurnOff;
            nudBlue.Enabled = !skypeStatus.TurnOff;
            btnColorPicker.Enabled = !skypeStatus.TurnOff;
            nudDelay.Enabled = !skypeStatus.TurnOff;
            cboAnimations.Enabled = !skypeStatus.TurnOff;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutForm aboutForm = new AboutForm();
            aboutForm.ShowDialog();
        }

        private void minimizeToTrayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings.Instance.MinimizeToTray = minimizeToTrayToolStripMenuItem.Checked;
        }

        private void startWithWindowsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            startWithWindows();            
        }

        private void startWithWindows()
        {
            try
            {
                Settings.Instance.WinStart = startWithWindowsToolStripMenuItem.Checked;

                Microsoft.Win32.RegistryKey rkApp = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                if (Settings.Instance.WinStart)
                {
                    if (rkApp.GetValue("DoNotDisturbIO", String.Empty).ToString() != Application.ExecutablePath.ToString())
                    {
                        rkApp.SetValue("DoNotDisturbIO", Application.ExecutablePath.ToString());
                    }
                }
                else
                {
                    rkApp.DeleteValue("DoNotDisturbIO", false);
                }
            }
            catch (Exception ex)
            {
                updateStatus(ex.Message);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dr = MessageBox.Show("Are you sure you wish to exit DoNotDisturbIO?", "Exit DoNotDisturbIO", MessageBoxButtons.YesNo);
                if (dr == System.Windows.Forms.DialogResult.No)
                {
                    return;
                }
                RUNNING = false;
                // Turn off the led strip
                writeMessage(SkypeStatus.TurnOffMessage());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (!RUNNING) { this.Close(); }
            }
        }

        private void trayIcon_MouseDoubleClick(object sender, EventArgs e)
        {
            if (!this.Visible)
            {
                this.Show();
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void statusTimer_Tick(object sender, EventArgs e)
        {
            tslStatus.Text = "";
            statusTimer.Stop();
            statusTimer.Enabled = false;

        }

        private void saveSettings()
        {
            if (cboComPort.SelectedItem != null)
            {
                Settings.Instance.DeviceInfo = (DeviceInfo)cboComPort.SelectedItem;
            }
            Settings.Instance.SaveSettings();
        }

        private void updateStatus(string status)
        {
            tslStatus.Text = status;
            statusTimer.Enabled = false;
            statusTimer.Enabled = true;
            statusTimer.Start();
        }


        private void statusSetup()
        {
            try
            {
                if (_skypeClient != null && _skypeClient.State == ClientState.SignedIn) { return; }
                _skypeClient = LyncClient.GetClient();                
            }
            catch (Exception ex)
            {
                updateStatus(String.Format("Skype error: {0}", ex.Message));
                _skypeClient = null;
            }

            if (_skypeClient != null && _skypeClient.State == ClientState.SignedIn)
            {
                _skypeClient.StateChanged += _skypeClient_StateChanged;
                _skypeClient.ConversationManager.ConversationAdded += ConversationManager_ConversationAdded;
                _skypeClient.Self.Contact.ContactInformationChanged += Contact_ContactInformationChanged;
                _skypeClient.ClientDisconnected += _skypeClient_ClientDisconnected;
                tsbtnSkype.Image = global::DoNotDisturbIO.Properties.Resources.green_circle;
                setCurrentLyncStatus();
            }
        }

        private void _skypeClient_ClientDisconnected(object sender, EventArgs e)
        {
            tsbtnSkype.Image = global::DoNotDisturbIO.Properties.Resources.red_circle;
        }

        private void _skypeClient_StateChanged(object sender, ClientStateChangedEventArgs e)
        {
            try
            {
                if (e.NewState == ClientState.SignedIn || e.NewState == ClientState.SigningIn || e.NewState == ClientState.Initializing)
                {
                    setCurrentLyncStatus();
                    tsbtnSkype.Image = global::DoNotDisturbIO.Properties.Resources.green_circle;
                }
                else
                {
                    tsbtnSkype.Image = global::DoNotDisturbIO.Properties.Resources.red_circle;
                    // Turn off the led strip
                    writeMessage(SkypeStatus.TurnOffMessage());
                }
            }
            catch (Exception ex)
            {
                updateStatus(ex.Message);
            }
        }

        private void Contact_ContactInformationChanged(object sender, ContactInformationChangedEventArgs e)
        {
            if (_skypeClient.State == ClientState.SigningOut || _skypeClient.State == ClientState.SignedOut || _skypeClient.State == ClientState.SigningIn)
            {
                return;
            }

            Contact self = sender as Contact;

            if (e.ChangedContactInformation.Contains(ContactInformationType.Availability))
            {
                ContactAvailability availability = (ContactAvailability)self.GetContactInformation(ContactInformationType.Availability);
                string activity = (string)self.GetContactInformation(ContactInformationType.Activity);
                Console.WriteLine(String.Format("{0} {1}", (int)availability, activity));
                setCurrentLyncStatus();
            }
        }

        private void ConversationManager_ConversationAdded(object sender, ConversationManagerEventArgs e)
        {
            try
            {
                var notified = false;
                var avModality = (AVModality)e.Conversation.Modalities[ModalityTypes.AudioVideo];


                while (e.Conversation.Modalities[ModalityTypes.AudioVideo].State == ModalityState.Notified &&
                       e.Conversation.State == ConversationState.Active)
                {
                    while (notified != true)
                    {
                        //We have an incomming call
                        if (avModality.State == ModalityState.Notified)
                        {
                            SkypeStatus skypeStatus = Settings.Instance.SkypeStatuses.Find(x => x.SkypeAvailability == SkypeAvailability.IncomingCall);
                            if (skypeStatus != null)
                            {
                                writeMessage(skypeStatus.ToString());
                            }

                            notified = true;
                        }
                    }
                }
                //Return user back to their current Sykpe State     
                setCurrentLyncStatus();
            }
            catch (Exception ex)
            {
                updateStatus(ex.Message);
            }
            
        }

        private void setCurrentLyncStatus()
        {
            try
            {
                _skypeClient = LyncClient.GetClient();

                if (_skypeClient != null && _skypeClient.State == ClientState.SignedIn && Settings.Instance.SkypeStatuses != null)
                {
                    ContactAvailability availability = (ContactAvailability)_skypeClient.Self.Contact.GetContactInformation(ContactInformationType.Availability);
                    SkypeStatus skypeStatus = Settings.Instance.SkypeStatuses.Find(x => x.SkypeAvailability == SkypeStatus.FromContactAvailability(availability));
                    if (skypeStatus != null)
                    {
                        writeMessage(skypeStatus.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                updateStatus(ex.Message);
            }
           
        }

        private void writeMessage(string message)
        {
            serialPort.ReadTimeout = 1000;
            serialPort.WriteTimeout = 1000;

            try
            {
                serialPort.Write(String.Format("{0}{1}{2}", SharedLib.START_CHAR, message, SharedLib.END_CHAR));
            }
            catch (Exception ex)
            {
                tsbtnCOM.Image = global::DoNotDisturbIO.Properties.Resources.red_circle;
                updateStatus(String.Format("COM error: {0}", ex.Message));
                serialPort.Close();
            }
            
        }

        private void setRGBDValue(int value, char RGBD)
        {
            SkypeStatus skypeStatus = (SkypeStatus)lsbxSkypeStatus.SelectedValue;
            switch (RGBD)
            {
                case 'R':
                    skypeStatus.Color.red = (byte)value;
                    break;
                case 'G':
                    skypeStatus.Color.green = (byte)value;
                    break;
                case 'B':
                    skypeStatus.Color.blue = (byte)value;
                    break;
                case 'D':
                    skypeStatus.Delay = value;
                    break;
            }
        }

        private void triggerCOM(bool close = false)
        {
            if (starting) { return; }
            try
            {
                if (serialPort.IsOpen)
                {
                    // Turn off the led strip
                    writeMessage(SkypeStatus.TurnOffMessage());
                    Thread.Sleep(1000);
                    serialPort.Close();
                    tsbtnCOM.Image = global::DoNotDisturbIO.Properties.Resources.red_circle;

                }
            }
            catch (Exception ex)
            {
                tsbtnCOM.Image = global::DoNotDisturbIO.Properties.Resources.red_circle;
                updateStatus(String.Format("COM error: {0}", ex.Message));
                serialPort.Close();
            }

            try
            {               
                if (!close)
                {
                    
                    DeviceInfo deviceInfo = (DeviceInfo)cboComPort.SelectedValue;
                    if (deviceInfo == null)
                    {
                        throw new Exception("No device ports available");
                    }
                    if (deviceInfo.DeviceType == DEVICE_TYPE.COM)
                    {
                        if (!serialPort.IsOpen)
                        {
                            serialPort.PortName = deviceInfo.Name;
                            serialPort.Open();
                            if (serialPort.IsOpen)
                            {
                                Settings.Instance.DeviceInfo = deviceInfo;
                                tsbtnCOM.Image = global::DoNotDisturbIO.Properties.Resources.green_circle;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                tsbtnCOM.Image = global::DoNotDisturbIO.Properties.Resources.red_circle;
                updateStatus(String.Format("COM error: {0}", ex.Message));
                serialPort.Close();
            }
        }
    }
}
