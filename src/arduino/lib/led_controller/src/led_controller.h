#ifndef LED_CONTROLLER_H
#define LED_CONTROLLER_H

#ifndef SHARED_LIB_H
#define SHARED_LIB_H 1
#endif

#include <Arduino.h>
#include <FastLED.h>
#include "../../../../csharp/DoNotDisturbIO/SharedLib.cs"

#define NUM_LEDS 15

#if ESP32
#define PIN 17
#else
#define PIN 6
#endif

#define LED_CHIPSET WS2812B
#define COLOR_ORDER GRB 

class LED_CONTROLLER
{
private:
  CRGB _leds[NUM_LEDS];
  void _setPixel(int pixel, byte red, byte green, byte blue);
  void _setAll(byte red, byte green, byte blue);
  void _fadeToBlack(int led_number, byte fade_value);
  void _fill(byte red, byte green, byte blue, int speed_delay, int from_led, int to_led);

public:    
  static uint8_t  hue8_rainbow;
  static uint8_t  hue8_breathe;

  LED_CONTROLLER();
  ~LED_CONTROLLER();

  void TurnOff();
  bool IsOff();

  // Animations
  void Solid(byte red, byte green, byte blue);
  void RainbowCycle(int speed_delay);
  void RunningLights(byte red, byte green, byte blue, int wave_delay);
  void PoliceStrobe(int speed_delay);
  void PoliceStrobe2(int speed_delay);
  void ShootingStar(byte red, byte green, byte blue, byte star_size, byte star_trail_decay, boolean trail_random_decay, int speed_delay);
  void FillFromLeft(byte red, byte green, byte blue, int speed_delay);
  void FillFromRight(byte red, byte green, byte blue, int speed_delay);
  void BreatheRainbow(int speed_delay);
  void FadeInOut(byte red, byte green, byte blue, int speed_delay = 0);
  void Sparkle(byte red, byte green, byte blue, int speed_delay);
  void SnowSparkle(byte red, byte green, byte blue, int speed_delay);
};

#endif

