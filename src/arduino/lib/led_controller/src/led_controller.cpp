#include <math.h>
#include "led_controller.h"

uint8_t LED_CONTROLLER::hue8_rainbow = 0;
uint8_t LED_CONTROLLER::hue8_breathe = 0;

static CEveryNMilliseconds RainbowCycleTrigger(30);

LED_CONTROLLER::LED_CONTROLLER() {
  FastLED.addLeds<LED_CHIPSET, PIN, COLOR_ORDER>(_leds, NUM_LEDS).setCorrection( LEDColorCorrection::UncorrectedColor );
}

LED_CONTROLLER::~LED_CONTROLLER() {
  
}

void LED_CONTROLLER::_setPixel(int pixel, byte red, byte green, byte blue) {
  _leds[pixel].r = red;
  _leds[pixel].g = green;
  _leds[pixel].b = blue;
}

void LED_CONTROLLER::_setAll(byte red, byte green, byte blue) {
  for(int i = 0; i < NUM_LEDS; i++ ) {
    _setPixel(i, red, green, blue); 
  }
  FastLED.show();;
}

void LED_CONTROLLER::_fadeToBlack(int led_number, byte fade_value) {
   _leds[led_number].fadeToBlackBy(fade_value);
}

void LED_CONTROLLER::_fill(byte red, byte green, byte blue, int speed_delay, int from_led, int to_led) {
  if (from_led < to_led) {
    for ( ; from_led < to_led; from_led++) {
      _setPixel(from_led, red, green, blue);
      FastLED.show();
      delay(speed_delay);
    }
  } else {
    for ( ; from_led > to_led; from_led--) {
      _setPixel(from_led, red, green, blue);
      FastLED.show();
      delay(speed_delay);
    }
  }
}

void LED_CONTROLLER::TurnOff() {
  _setAll(0, 0, 0);
}

bool LED_CONTROLLER::IsOff() {
  bool off = true;  
  for (int i = 0; i < NUM_LEDS; i++) {
    if ( _leds[i] ) {
      off = false;
      break;
    }
  }
  return off;
}

void LED_CONTROLLER::Solid(byte red, byte green, byte blue) {
  _setAll(red, green, blue);
  FastLED.show();
}

void LED_CONTROLLER::RainbowCycle(int speed_delay) {
  RainbowCycleTrigger.setPeriod(speed_delay);
  fill_rainbow( _leds, NUM_LEDS, hue8_rainbow, 7);
  FastLED.show();
  if (RainbowCycleTrigger) {
    hue8_rainbow++;
    RainbowCycleTrigger.reset();
  }

}

void LED_CONTROLLER::RunningLights(byte red, byte green, byte blue, int wave_delay) {
  int Position = 0;
  
  for(int i = 0; i < NUM_LEDS * 2; i++)
  {
      Position++; // = 0; //Position + Rate;
      for(int i = 0; i < NUM_LEDS; i++) {
        _setPixel(i, ((sin(i + Position) * 127 + 128) / 255) * red,
                     ((sin(i + Position) * 127 + 128) / 255) * green,
                     ((sin(i + Position) * 127 + 128) / 255) * blue);
      }      
      FastLED.show();
      delay(wave_delay);
  }
}

void LED_CONTROLLER::PoliceStrobe(int speed_delay) {
  if (speed_delay < 50) { speed_delay = 50; } // lower than 50 is pointless
  int midlights[2];
  if (NUM_LEDS % 2 == 0) { // even # of lights
    midlights[0] = (floor(NUM_LEDS/2) - 1);
    midlights[1] = midlights[0] + 1;
  } else { // odd # of lights
    midlights[0] = (int)((1 + NUM_LEDS ) / 2) - 1;
    midlights[1] = midlights[0];
  }

  for (int j = 0; j < 3; j++) {

    // blue strobe
    _setAll(0, 0, 0);
    for (int i = 0; i < midlights[0]; i++) {      
      _setPixel(i, 0, 0, 255);      
    }
    if (j % 2 == 0) {
      _setPixel(midlights[0], 255, 255, 255);
      _setPixel(midlights[1], 255, 255, 255);
    }
    FastLED.show();
    delay(speed_delay);
    TurnOff();
    FastLED.show();
    delay(speed_delay);
  }

  for (int j = 0; j < 3; j++) {
    // red strobe
    _setAll(0, 0, 0);
    for (int i = midlights[1] + 1; i < NUM_LEDS; i++) {      
      _setPixel(i, 255, 0, 0);      
    }
    if (j % 2 == 0) {
      _setPixel(midlights[0], 255, 255, 255);
      _setPixel(midlights[1], 255, 255, 255);
    }
    FastLED.show();
    delay(speed_delay);
    TurnOff();
    FastLED.show();
    delay(speed_delay);
  }
}

void LED_CONTROLLER::PoliceStrobe2(int speed_delay) {
  if (speed_delay < 50) { speed_delay = 50; } // lower than 50 is pointless
  int midlights[2];
  if (NUM_LEDS % 2 == 0) { // even # of lights
    midlights[0] = (floor(NUM_LEDS/2) - 1);
    midlights[1] = midlights[0] + 1;
  } else { // odd # of lights
    midlights[0] = (int)((1 + NUM_LEDS) / 2) - 1;
    midlights[1] = midlights[0];
  }

  for (int j = 0; j < 2; j++) {

    // blue strobe
    TurnOff();
    for (int i = 0; i < midlights[0]; i++) {      
      _setPixel(i, 0, 0, 255);      
    }
    FastLED.show();
    delay(speed_delay);
    TurnOff();
    for (int i = midlights[1] + 1; i < NUM_LEDS; i++) {      
      _setPixel(i, 255, 255, 255);      
    }
    FastLED.show();
    delay(speed_delay);
  }

  for (int j = 0; j < 2; j++) {
    // red strobe
    TurnOff();
    for (int i = midlights[1] + 1; i < NUM_LEDS; i++) {      
      _setPixel(i, 255, 0, 0);      
    }
    FastLED.show();
    delay(speed_delay);
    TurnOff();
    for (int i = 0; i < midlights[0]; i++) {      
      _setPixel(i, 255, 255, 255);      
    }
    FastLED.show();
    delay(speed_delay);
  }
}

void LED_CONTROLLER::ShootingStar(byte red, byte green, byte blue, byte star_size, byte star_trail_decay, boolean trail_random_decay, int speed_delay) {  
  
  TurnOff();
  
  for(int i = 0; i < NUM_LEDS+NUM_LEDS; i++) {
    
    // fade brightness all LEDs one step
    for(int j = 0; j < NUM_LEDS; j++) {
      if( (!trail_random_decay) || (random(10) > 5) ) {
        _fadeToBlack(j, star_trail_decay );        
      }
    }
    
    // draw shooting star
    for(int j = 0; j < star_size; j++) {
      if( ( i - j < NUM_LEDS) && ( i - j >= 0) ) {
        _setPixel(i - j, red, green, blue);
      } 
    }
   
    FastLED.show();
    delay(speed_delay);
  }
}

void LED_CONTROLLER::FillFromLeft(byte red, byte green, byte blue, int speed_delay) {
  TurnOff();
  _fill(red, green, blue, speed_delay, 0, NUM_LEDS - 1);
  delay(speed_delay);
  _fill(0, 0, 0, speed_delay, NUM_LEDS - 1, 0);
}

void LED_CONTROLLER::FillFromRight(byte red, byte green, byte blue, int speed_delay) {
  TurnOff();
  _fill(red, green, blue, speed_delay, NUM_LEDS - 1, 0);
  delay(speed_delay);
  _fill(0, 0, 0, speed_delay, 0, NUM_LEDS - 1);
  
}

void LED_CONTROLLER::BreatheRainbow(int speed_delay) {
  TurnOff();
   
  int pos = beatsin16(speed_delay, 30, 255); // generating the sinwave 
  fill_solid(_leds, NUM_LEDS, CHSV( hue8_breathe, 255, pos)); // CHSV (hue, saturation, value);
  FastLED.show();
  EVERY_N_MILLISECONDS(100) {hue8_breathe++;}
}

void LED_CONTROLLER::FadeInOut(byte red, byte green, byte blue, int speed_delay) {
  float r, g, b = 0;
      
  for(int i = 0; i < 256; i++) { 
    r = (i / 256.0) * red;
    g = (i / 256.0) * green;
    b = (i / 256.0) * blue;
    _setAll(r, g, b);
    FastLED.show();
    delay(speed_delay);
    i++;
  }
  delay(5);
     
  for(int i = 255; i >= 0; i--) {
    r = (i / 256.0) * red;
    g = (i / 256.0) * green;
    b = (i / 256.0) * blue;
    _setAll(r, g, b);
    FastLED.show();
    delay(speed_delay);
    i--;
  }
}

void LED_CONTROLLER::Sparkle(byte red, byte green, byte blue, int speed_delay) {
  int pixel = random(NUM_LEDS);
  _setPixel(pixel, red, green, blue);
  FastLED.show();
  delay(speed_delay);
  _setPixel(pixel, 0, 0, 0);
}

void LED_CONTROLLER::SnowSparkle(byte red, byte green, byte blue, int speed_delay) {
  _setAll(red,green,blue);
  
  int pixel = random(NUM_LEDS);
  _setPixel(pixel, 0xff, 0xff, 0xff);
  FastLED.show();
  delay(20);
  _setPixel(pixel, red, green, blue);
  FastLED.show();
  delay(speed_delay);
}

