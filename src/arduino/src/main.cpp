#include <Arduino.h>
#include <led_controller.h>
#if ESP32
#include <BluetoothSerial.h>
#endif

LED_CONTROLLER led_controller;
#if ESP32
BluetoothSerial SerialBT;
#endif

void parseData();
void readSerialData();

int animation = Animations::Solid;
byte red = 0;
byte green = 0;
byte blue = 0;
int speed_delay = 30;
bool turn_off = false;


const byte BUFF_SIZE = 100;
char inputBuffer[BUFF_SIZE];
byte bytesRecvd = 0;
bool readInProgress = false;
int cnt = 0;

void setup()
{
  Serial.setTimeout(2000);
  Serial.begin(9600); // BaudRate: 9600, Parity: None, DataBits: 8, StopBits: 1
#if ESP32
  SerialBT.begin("DoNotDisturbIO");
#endif
  
  while(!Serial);
}

void parseData() {

    // split the data into its parts    
  char * str_token_idx; // this is used by strtok() as an index

  str_token_idx = strtok(inputBuffer,","); 
  animation = atoi(str_token_idx);
  str_token_idx = strtok(NULL, ",");
  red = atoi(str_token_idx);
  str_token_idx = strtok(NULL, ",");
  green = atoi(str_token_idx);
  str_token_idx = strtok(NULL, ",");
  blue = atoi(str_token_idx);
  str_token_idx = strtok(NULL, ",");
  speed_delay = atoi(str_token_idx);
  str_token_idx = strtok(NULL, ",");
  turn_off = atoi(str_token_idx);

}

void readSerialData() {
    // receive data from PC and save it into inputBuffer    
#if ESP32
  while((SerialBT.available() > 0)) {
    char c = SerialBT.read();
#else
  while((Serial.available() > 0)) {
    char c = Serial.read();
#endif
      // the order of these IF clauses is significant      
    if (c == END_CHAR) {
      readInProgress = false;
      inputBuffer[bytesRecvd] = 0;
      parseData();
    }
    
    if(readInProgress) {
      inputBuffer[bytesRecvd] = c;
      bytesRecvd++;
      if (bytesRecvd == BUFF_SIZE) {
        bytesRecvd = BUFF_SIZE - 1;
      }
    }

    if (c == START_CHAR) { 
      bytesRecvd = 0; 
      readInProgress = true;
    }
  }
}

void loop() {
  readSerialData();
  Serial.flush();

  
  if (turn_off) { 
    led_controller.TurnOff();
    delay(30);
  } else {

    switch(animation) {
      case Animations::None:
        led_controller.Solid(red, green, blue);
        break;
      case Animations::Solid:
        led_controller.Solid(red, green, blue);
        break;
      case Animations::RainbowCycle:
        led_controller.RainbowCycle(speed_delay);
        break;
      case Animations::RunningLights:
        led_controller.RunningLights(red, green, blue, speed_delay);
        break;
      case Animations::PoliceStrobe:
        led_controller.PoliceStrobe(speed_delay);
        break;
      case Animations::PoliceStrobe2:
        led_controller.PoliceStrobe2(speed_delay);
        break;
      case Animations::ShootingStar:
        led_controller.ShootingStar(red, green, blue, 2, 65, true, speed_delay);
        break;
      case Animations::FillFromLeft:
        led_controller.FillFromLeft(red, green, blue, speed_delay);
        break;
      case Animations::FillFromRight:
        led_controller.FillFromRight(red, green, blue, speed_delay);
        break;
      case Animations::BreatheRainbow:
        led_controller.BreatheRainbow(speed_delay);
        break;
      case Animations::FadeInOut:
        led_controller.FadeInOut(red, green, blue, speed_delay);
        break;
      case Animations::Sparkle:
        led_controller.Sparkle(red, green, blue, speed_delay);
        break;
      case Animations::SnowSparkle:
        led_controller.SnowSparkle(red, green, blue, speed_delay);
        break;
    }
  }
}